from flask import Flask, jsonify
from view import getList



app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

app.add_url_rule('/list', 'list', getList, methods=['GET'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)