from flask import request, jsonify
import pymysql
from redis import Redis
import time


r = Redis(host='my_redis', port=6379, decode_responses=True, charset='UTF-8', encoding='UTF-8')

config = {
    # 'host': 'localhost',
    'host': 'webdb',
    'port': 3306,
    'user': 'root',
    'password': '123456',
    'db': 'webserver',
    'charset': 'utf8mb4'
}

def query(sql):
    conn = pymysql.connect(**config)
    cursor = conn.cursor()
    cursor.execute(sql)
    res = cursor.fetchall()
    conn.commit()
    conn.close()
    return res

def getUserInfo(username):  # reserved
    userId = query('select uid from user where username={};'.format(username))[0][0]
    name, sex = query('select name, sex from user_info where id={};'.format(userId))[0]
    return name, sex
    
def getBuildings():
    buildings = query('select id, name from building;')
    return buildings

def getDorms(building_id, sex):
    units = query('select id from unit where building_id={};'.format(building_id))
    units = [str(i[0]) for i in units]
    maxCapacity = 8
    dorms = {}
    for i in range(1, maxCapacity + 1):
        sql = 'select count(*) from dorm where sex="{}" and unit_id in ({}) and capacity={};'.format(sex, ', '.join(units), i)
        capacity = sum([cap[0] for cap in query(sql)])
        dorms[i] = capacity
    return dorms

def setRedis():
    buildings = getBuildings()
    dataMale = [{'name': building[1].encode('utf-8').decode('utf-8'), 'dorm': getDorms(building[0], '男')} for building in buildings]
    dataFemale = [{'name': building[1].encode('utf-8').decode('utf-8'), 'dorm': getDorms(building[0], '女')} for building in buildings]
    r.delete('男')
    r.delete('女')
    for building in dataMale:
        r.lpush('男', building['name'])
        r.hmset(building['name'] + '男', building['dorm'])
    for building in dataFemale:
        r.lpush('女', building['name'])
        r.hmset(building['name'] + '女', building['dorm'])
    sql = 'select dorm.id as dorm_id, capacity, sex, building.name building_name from dorm, unit, building where dorm.unit_id=unit.id and building_id=building.id;'
    res = query(sql)
    for i in res:
        r.delete(i[3] + i[2] + str(i[1]))
    for i in res:
        r.lpush(i[3] + i[2] + str(i[1]), i[0])
    buildings = query('select id, name from building;')
    for building in buildings:
        r.set(building[1], building[0])
    sql = 'select username, user_info.id id, sex,authentication from user, user_info, user_authentication where user.uid=user_info.id and user_authentication.uid=user_info.id;'
    res = query(sql)
    for i in res:
        r.set(i[0] + 'ID', i[1])
        r.set(i[0] + 'SEX', i[2])
        r.set(i[3] + 'AUTH', i[0])
    
def getList():
    data = {}
    data['buildings_male'] = []
    for building in r.lrange('男', 0, 100):
        temp = {}
        temp['name'] = building
        temp['dorm'] = {}
        for i in r.hkeys(building + '男'):
            temp['dorm'][int(i)] = int(r.hmget(building + '男', i)[0])
        data['buildings_male'].append(temp)

    data['buildings_female'] = []
    for building in r.lrange('女', 0, 100):
        temp = {}
        temp['name'] = building
        temp['dorm'] = {}
        for i in r.hkeys(building + '女'):
            temp['dorm'][int(i)] = int(r.hmget(building + '女', i)[0])
        data['buildings_female'].append(temp)
    print(data)
    return jsonify(data)

time.sleep(15)
setRedis()
# getList()