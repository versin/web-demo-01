from flask import Flask
from view import order, getBuilding, hasDorm


app = Flask(__name__)

app.add_url_rule('/order', 'order', order, methods=['POST'])
app.add_url_rule('/building', 'building', getBuilding, methods=['GET'])
app.add_url_rule('/hasDorm', 'hasDorm', hasDorm, methods=['POST'])


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7000, debug=True)