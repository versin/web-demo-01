from flask import request, jsonify
import pymysql
import random
import datetime
from redis import Redis
import time


r = Redis(host='my_redis', port=6379, decode_responses=True, charset='UTF-8', encoding='UTF-8')

config = {
    # 'host': 'localhost',
    'host': 'webdb',
    'port': 3306,
    'user': 'root',
    'password': '123456',
    'db': 'webserver',
    'charset': 'utf8mb4'
}

def query(sql):
    conn = pymysql.connect(**config)
    cursor = conn.cursor()
    cursor.execute(sql)
    res = cursor.fetchall()
    conn.commit()
    conn.close()
    return res

def insert(sql):
    conn = pymysql.connect(**config)
    cursor = conn.cursor()
    cursor.execute(sql)
    cursor.execute('select LAST_INSERT_ID();')
    id = cursor.fetchone()[0]
    conn.commit()
    conn.close()
    return id

def update(sql):
    conn = pymysql.connect(**config)
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()
    conn.close()


def checkDorm(userId):
    dorm = query('select dorm_id from user_dorm where uid={};'.format(userId))
    return bool(dorm)

def hasDorm():
    username = request.form['username']
    userId = query('select uid from user where username={};'.format(username))[0][0]
    dormId = query('select dorm_id from user_dorm where uid={};'.format(userId))
    if not dormId:
        return jsonify(code=200)
    dormId = dormId[0][0]
    unitId, dormName = query('select unit_id, name from dorm where id={};'.format(dormId))[0]
    buildingId, unitName = query('select building_id, name from unit where id={};'.format(unitId))[0]
    buildingName = query('select name from building where id={};'.format(buildingId))[0][0]
    return jsonify({'code': 520, 'buildingName': buildingName, 'unitName': unitName, 'dormName': dormName})

def getBuilding():
    building = query('select name from building;')
    building = [i[0] for i in building]
    return jsonify(building)

def order():
    num = 1
    username = request.form['username']
    building = request.form['building']

    userId = r.get(username + 'ID')
    sex = r.get(username + 'SEX')
    roommateIds = []
    if request.form.get('bind') and request.form['auth']:
        authentications = request.form['auth']
        authentications = [authentication.split('=')[1] for authentication in authentications.split('&')]
        print(authentications)
        for index, auth in enumerate(authentications):
            roommateId = r.get(auth + 'AUTH')
            print('roommateId', roommateId)
            if not roommateId:
                return jsonify(code=401)  # 认证码不正确
            roommateId = roommateId[0][0]
            if userId == roommateId:
                return jsonify(code=402)  # 可不能拿自己的认证码

            roommateSex = r.get(roommateId + 'SEX')
            if sex != roommateSex:
                return jsonify(code=403)  # 性别不同
            if checkDorm(roommateId):
                return jsonify(code=405)  # 同住人已有宿舍
            if roommateId not in roommateIds:
                roommateIds.append(roommateId)
        num += len(roommateIds)

    dormsId = {}
    maxCapacity = 8
    for i in range(num, maxCapacity + 1):
        dorms = r.lrange(building + sex + str(i), 0, 100)
        if dorms:
            dormsId[i] = dorms
    print(dormsId)
    if not dormsId:
        return jsonify(code=404)
    buildingId = r.get(building)
    capacity = random.choice(list(dormsId.keys()))
    dormId = random.choice(dormsId[capacity])
    print('hello', capacity, dormId)
    r.lrem(building + sex + str(capacity), 1, dormId)
    r.lpush(building + sex + str(capacity - num), dormId)
    r.hset(building + sex, str(capacity), int(r.hget(building + sex, str(capacity))) - 1)
    r.hset(building + sex, str(capacity - num), int(r.hget(building + sex, str(capacity - num))) + 1)

    orderId = insert('insert into order_table(uid, building_id, num, sex, sussess, submit_time) values({}, {}, {}, "{}", {}, "{}");'.format(userId, buildingId, num, sex, 'True', datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    insert('insert into order_info(order_id) values({});'.format(orderId))
    insert('insert into user_dorm(uid, dorm_id, create_time, is_valid) values({}, {}, "{}", {});'.format(userId, dormId, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'True'))
    update('update dorm set capacity=capacity-{} where id={};'.format(num, dormId))
    for roommateId in roommateIds:
        insert('insert into order_info(order_id, roommate_id) values({}, {});'.format(orderId, roommateId))
        insert('insert into user_dorm(uid, dorm_id, create_time, is_valid) values({}, {}, "{}", {});'.format(roommateId, dormId, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'True'))
    return jsonify(code=200)