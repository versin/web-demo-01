import pymysql
import hashlib


config = {
    # 'host': 'localhost',
    'host': 'webdb',
    'port': 3306,
    'user': 'root',
    'password': '123456',
    'db': 'webserver',
    'charset': 'utf8mb4'
}


def existUsername(username):
    conn = pymysql.connect(**config)
    cursor = conn.cursor()
    sql = 'select * from user where username="{}";'.format(username)
    cursor.execute(sql)
    res = cursor.fetchone()
    conn.commit()
    conn.close()
    return bool(res)

def insertData(username, password, sex):
    conn = pymysql.connect(**config)
    cursor = conn.cursor()
    sql = 'insert into user_info(name, stu_no, sex, mail) values("{}", "{}", "{}", "{}@pku.edu.cn");'.format(username, username, sex, username)  
    cursor.execute(sql)
    cursor.execute('select LAST_INSERT_ID();')
    userId = cursor.fetchone()[0]
    sql = 'insert into user(uid, username, password, is_del) values({}, "{}", "{}", False);'.format(userId, username, password)
    cursor.execute(sql)
    sql = 'insert into user_authentication(uid, authentication) values("{}", "{}");'.format(userId, username)
    cursor.execute(sql)
    conn.commit()
    conn.close()

def encodePassword(password):
    first = hashlib.md5(password.encode(encoding='utf-8')).hexdigest()
    second = hashlib.md5(first.encode(encoding='utf-8')).hexdigest()
    return second