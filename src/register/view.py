from flask import request, jsonify
from managedb import existUsername, encodePassword, insertData


def register():
	username = request.form['username']
	password = request.form['password']
	sex = request.form['sex']
	print(request.form)
	if existUsername(username):  # 检测该账号是否被注册
		return jsonify(code=401)
	password = encodePassword(password)
	insertData(username, password, sex)
	return jsonify(code=200)