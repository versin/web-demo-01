from flask import Flask
from view import register


app = Flask(__name__)

app.add_url_rule('/register', 'register', register, methods=['POST'])


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000, debug=True)