import pika
import requests
import time
import json


time.sleep(15)

credentials = pika.PlainCredentials('user01', '123456')  # mq用户名和密码
# 虚拟队列需要指定参数 virtual_host，如果是默认的可以不填。
connection = pika.BlockingConnection(pika.ConnectionParameters(host='mq', port=5672, virtual_host='/', credentials=credentials))
channel = connection.channel()
# 申明消息队列，消息在这个队列传递，如果不存在，则创建队列
channel.queue_declare(queue='mq', durable=False)
# 定义一个回调函数来处理消息队列中的消息，这里是打印出来
def callback(ch, method, properties, body):
    ch.basic_ack(delivery_tag=method.delivery_tag)
    data = json.loads(body.decode())
    print(data, type(data))
    requests.post(url='http://order:7000/order', data=data)

# 告诉rabbitmq，用callback来接收消息
channel.basic_consume('order', callback)
# 开始接收信息，并进入阻塞状态，队列里有信息才会调用callback进行处理
channel.start_consuming()