const express = require('express')
const ejs = require('ejs')
const path = require('path')
const http = require('http')
const querystring = require('querystring')
const cookieParser=require("cookie-parser")
const axios = require('axios')
const app = module.exports = express()
// const amqp = require('amqplib')
var amqp = require('amqplib/callback_api')

app.set('views', './views')
app.engine('html', ejs.__express)
app.set('view engine', 'html')
app.use(express.static(path.join(__dirname, 'static')))
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())


// let rabbitmq = {
//     hostname: "mq",
//     port: "5672",
//     username: "user01",
//     password: "123456",
//     authMechanism: "AMQPLAIN" ,
//     pathname: "/",
//     ssl: { 
//         enabled : false  
//     }  
// }

const topic = 'order';
function publisher(conn, data) {
    conn.createChannel(on_open);
    function on_open(err, ch) {
        if (err != null) bail(err);
        ch.assertQueue(topic);
        ch.sendToQueue(topic, new Buffer(JSON.stringify(data)));
    }
}

// 宿舍列表信息即首页
app.get('/', (req, res) => {
	let token = req.cookies.token
	if(!token)  // 未登录状态
		return res.redirect('/login')
	axios.post('http://jwt:6000/decode', querystring.stringify({'token': token}))
		.then((jwtRes) => {
			if(jwtRes.data.code != 200)  // 未登录状态
				return res.redirect('/login')
			let username = jwtRes.data.data.username
			axios.get('http://dorm:5000/list')
				.then((listRes) => {
					res.render('homepage', {
						username: username,
						male: listRes.data.buildings_male,
						female: listRes.data.buildings_female
					})
				})
		})
})


// 响应登录页面
app.get('/login', (req, res) => {
	let token = req.cookies.token
	if(token) {
		axios.post('http://jwt:6000/decode', querystring.stringify({'token': token}))
			.then((jwtRes) => {
				if(jwtRes.data.code == 200)  // 登录状态跳转首页
					return res.redirect('/')
				else
					return res.render('login', {tip: ''})
			})
	}
	else
		res.render('login', {tip: ''})
})

// 登录
app.post('/login', (req, res) => {
	axios.post('http://login:4000/login', querystring.stringify(req.body))
		.then((loginRes) => {
  			if(loginRes.data.code == 200) {
  				axios.post('http://jwt:6000/encode', querystring.stringify(req.body))
  					.then((jwtRes) => {
  						res.cookie('token', jwtRes.data.token, {maxAge: 3600000})  // 将 token 存在 cookie 里，有效期为 1h
  						res.redirect('/')
  					})
  			}
  			else {
  				res.render('login', {tip: '用户名不存在或密码错误！！！'})
  			}
		})
})


// 响应注册页面
app.get('/register', (req, res) => {
	res.render('register', {tip: ''})
})

// 注册
app.post('/register', (req, res) => {
	if(req.body.password != req.body.confirmPassword)
		res.render('register', {tip: '两次输入的密码不一致！！！'})
	else {
		axios.post('http://register:3000/register', querystring.stringify(req.body))
			.then((registerRes) => {
				if(registerRes.data.code == 200) {
					axios.post('http://jwt:6000/encode', querystring.stringify({
						username: req.body.username,
						password: req.body.password
					}))
	  					.then((jwtRes) => {
	  						res.cookie('token', jwtRes.data.token, {maxAge: 3600000})  // 将 token 存在 cookie 里，有效期为 1h
	  						res.redirect('/')
	  					})
				}
				else {
					return res.render('register', {tip: '账号已存在！！！'})
				}
			})
	}
})


// 注销
app.get('/logout', (req, res) => {
	res.clearCookie('token')
	res.redirect('/login')
})


// 响应订单页面
app.get('/order', (req, res) => {
	let token = req.cookies.token
	if(!token)  // 未登录状态
		return res.redirect('/login')
	axios.post('http://jwt:6000/decode', querystring.stringify({'token': token}))
		.then((jwtRes) => {
			if(jwtRes.data.code != 200)  // 未登录状态
				return res.redirect('/login')
			let username = jwtRes.data.data.username
			axios.post('http://order:7000/hasDorm', querystring.stringify({username: username}))
				.then((dormRes) => {
					if(dormRes.data.code == 520)
						return res.render('orderDetail', {
							username: username,
							buildingName: dormRes.data.buildingName,
							unitName: dormRes.data.unitName,
							dormName: dormRes.data.dormName
						})
					axios.get('http://order:7000/building')
						.then((buildingRes) => {
							res.render('order', {buildings: buildingRes.data, username: username, tip: ''})
						})
				})
		})	
})

// 订单
app.post('/order', (req, res) => {
	let username = req.body.username
	let building = req.body.building
	let bind = req.body.bind
	let auth = []
	if(req.body.auth1)
		auth.push(req.body.auth1)
	if(req.body.auth2)
		auth.push(req.body.auth2)
	if(req.body.auth3)
		auth.push(req.body.auth3)
	auth = querystring.stringify(auth)
	data = {
		username: username,
		building: building,
		bind: bind,
		auth: auth
	}
	amqp.connect('amqp://user01:123456@mq', function(err, conn) {
	    publisher(conn, data)
	    setTimeout(function() {
			axios.post('http://order:7000/hasDorm', querystring.stringify({username: username}))
				.then((dormRes) => {
					if(dormRes.data.code == 520)
						return res.render('orderDetail', {
							username: username,
							buildingName: dormRes.data.buildingName,
							unitName: dormRes.data.unitName,
							dormName: dormRes.data.dormName
						})
					else {
						setTimeout(function() {
							axios.post('http://order:7000/hasDorm', querystring.stringify({username: username}))
								.then((dormRes) => {
									if(dormRes.data.code == 520)
										return res.render('orderDetail', {
											username: username,
											buildingName: dormRes.data.buildingName,
											unitName: dormRes.data.unitName,
											dormName: dormRes.data.dormName
										})
									else {
										axios.get('http://order:7000/building')
											.then((buildingRes) => {
												res.render('order', {buildings: buildingRes.data, username: username, tip: ''})
											})
									}
								})
						}, 1000)
					}
				})
		}, 1000)
	})
})


let port = 8000
app.listen(port)
console.log(`http://127.0.0.1:${port}`)